package main;

import java.util.List;

import excel.*;

public class Hospital {
	private List<Nurse> nurses;
	private List<Department> departments;
	private WriteExcel writeExcel;
	
	public float score;
	public float cost;
	
	public Hospital() throws Exception {
		ReadExcel readExcel = new ReadExcel();
		
		
		nurses = readExcel.getNurses();
		departments = readExcel.getDepartments();
		writeExcel = new WriteExcel();
	}
	
	public List<Nurse> getNurses() {
		return nurses;
	}

	public List<Department> getDepartments() {
		return departments;
	}

	public void Calculate() {
		
		
		
		try {
			writeExcel.WriteFile();
			writeExcel.WriteNurses(nurses);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unused")
	private void CalculateScore() {
		
	}
	
	@SuppressWarnings("unused")
	private void CalculateNumbers() {
		
	}
	
	@SuppressWarnings("unused")
	private void OptimizeScore() {
		
	}
	
	@SuppressWarnings("unused")
	private void FillDepartments() {
		
	}
	
	@SuppressWarnings("unused")
	private void CalculateCost() {
		
	}
}
