package main;
import java.util.ArrayList;
import java.util.List;

import types.*;

public class Nurse {
	public String name;
	public List<Preference> dayPrefences;
	public NurseType type;
	public int switchingPenalty;
	public int departmentPenalty;
	
	//public int workingHours;
	//public int remainingWorkingHours;
	
	public Nurse(String name) {
		this.name = name;
		dayPrefences = new ArrayList<Preference>();
	}
	
	public int getSwitchingPenalty() {
		return switchingPenalty;
	}

	public void setSwitchingPenalty(int switchingPenalty) {
		this.switchingPenalty = switchingPenalty;
	}

	public int getDepartmentPenalty() {
		return departmentPenalty;
	}

	public void setDepartmentPenalty(int departmentPenalty) {
		this.departmentPenalty = departmentPenalty;
	}

	public NurseType getType() {
		return type;
	}

	public void setType(NurseType type) {
		this.type = type;
	}
	
	public void setType(int typeId) {
		this.type = NurseType.values()[typeId - 1];
	}

	public String getName() {
		return name;
	}

	public List<Preference> getDayPrefences() {
		return dayPrefences;
	}

	public void setDayPrefences(List<Preference> dayPrefences) {
		this.dayPrefences = dayPrefences;
	}
}
