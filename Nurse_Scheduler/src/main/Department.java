package main;

public class Department {
	public String name;
	private int[] amountNurses;
	
	public Department(String name) {
		this.name = name;
		amountNurses = new int[8];
	}

	public int[] getAmountNurses() {
		return amountNurses;
	}

	public void setAmountNurses(int[] amountNurses) {
		this.amountNurses = amountNurses;
	}
	
	public void setAmountNurses(int E1, int E2, int D1, int D2, int L1, int L2, int N1, int N2) {
		amountNurses[0] = E1;
		amountNurses[1] = E2;
		amountNurses[2] = D1;
		amountNurses[3] = D2;
		amountNurses[4] = L1;
		amountNurses[5] = L2;
		amountNurses[6] = N1;
		amountNurses[7] = N2;
	}

	public String getName() {
		return name;
	}
	
}
