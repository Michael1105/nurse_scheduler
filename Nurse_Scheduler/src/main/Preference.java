package main;

public class Preference {
	public int[] priorities;
	public int free;
	
	public Preference(int E1, int E2, int D1, int D2, int L1, int L2, int N1, int N2, int free) {
		priorities = new int[8];
		priorities[0] = checkRange(E1,0,100);
		priorities[1] = checkRange(E2,0,100);
		priorities[2] = checkRange(D1,0,100);
		priorities[3] = checkRange(D2,0,100);
		priorities[4] = checkRange(L1,0,100);
		priorities[5] = checkRange(L2,0,100);
		priorities[6] = checkRange(N1,0,100);
		priorities[7] = checkRange(N2,0,100);
		free = checkRange(free, 0, 9);
	}
	
	public Preference(int[] priorities, int free) {
		this.priorities = priorities;
		this.free = free;
	}
	
	public int[] getPriorities() {
		return priorities;
	}

	public void setPriorities(int[] priorities) {
		this.priorities = priorities;
	}

	public int getFree() {
		return free;
	}

	public void setFree(int free) {
		this.free = free;
	}

	private int checkRange(int value, int min, int max) {
		if (value < min)
			return min;
		else if (value > max)
			return max;
		else
			return value;
	}
}
