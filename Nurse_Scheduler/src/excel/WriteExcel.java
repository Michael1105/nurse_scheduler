package excel;

import java.io.*;
import java.util.*;

import main.Nurse;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.*;
import jxl.write.Number;

public class WriteExcel {
	private static String fileName = "output.xls";
	private static String nurseSheetName = "Nurse";

	private WritableWorkbook workbook;
	private WritableSheet nurseSheet;


	public WriteExcel() {
		File file = new File(fileName);
		WorkbookSettings wbSettings = new WorkbookSettings();
		wbSettings.setLocale(new Locale("en", "EN"));
		
	    try {
			workbook = Workbook.createWorkbook(file, wbSettings);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    workbook.createSheet(nurseSheetName, 0);
	    nurseSheet = workbook.getSheet(0);
	}
	
	public void WriteFile() throws Exception {
		if (nurseSheet.getColumns() == 0 || nurseSheet.getRows() == 0)
			throw new Exception("Please write nurse sheet before writing file");
		
		workbook.write();
	    workbook.close();
	}
	
	public void WriteNurses(List<Nurse> nurses) {
		Label nurseName;
		Number departmentPenalty;
		Number switchingPenalty;
		
		
		for (Nurse nurse : nurses) {
			nurseName = new Label(0, nurses.indexOf(nurse), nurse.getName());
			departmentPenalty = new Number(1, nurses.indexOf(nurse), nurse.getDepartmentPenalty());
			switchingPenalty = new Number(2, nurses.indexOf(nurse), nurse.getSwitchingPenalty());
			try {
				nurseSheet.addCell(nurseName);
				nurseSheet.addCell(departmentPenalty);
				nurseSheet.addCell(switchingPenalty);
			} catch (WriteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
}
