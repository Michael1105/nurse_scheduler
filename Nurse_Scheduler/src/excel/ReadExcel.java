package excel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import main.Department;
import main.Nurse;
import main.Preference;

public class ReadExcel{
	private static String fileName = "input.xls";
	private static String nurseSheetName = "Nurse";
	private static String departmentSheetName = "Department";

	private Sheet nurseSheet;
	private Sheet departmentSheet;

	public ReadExcel() throws Exception {
		File file = new File(fileName);
		Workbook excelFile;
		try {
			excelFile = Workbook.getWorkbook(file);
		} catch(Exception ex) {
			ex.printStackTrace();
			throw new Exception("Can't find or open '" + fileName +"'");
		}
		try {
			nurseSheet = excelFile.getSheet(nurseSheetName);
			nurseSheet.getName();
		} catch(Exception ex) {
			ex.printStackTrace();
			throw new Exception("Can't find or open sheet '" + nurseSheetName +"'");
		}
		try {
			departmentSheet = excelFile.getSheet(departmentSheetName);
		} catch(Exception ex) {
			ex.printStackTrace();
			throw new Exception("Can't find or open sheet '" + departmentSheetName +"'");
		}	
	}
	
	public List<Nurse> getNurses() throws Exception {
		List<Nurse> nurses = new ArrayList<Nurse>();
		
		try {
			List<Preference> prefereces;

			Nurse nurse;
			for (int row = 2; row < nurseSheet.getRows(); row++) {
				nurse = new Nurse(nurseSheet.getCell(0, row).getContents());
				
				prefereces = new ArrayList<Preference>();
				Preference preference;
				int column = 1;
				while(column + 9 < nurseSheet.getColumns()) {
					preference = new Preference
						(
							Integer.parseInt(nurseSheet.getCell(column, row).getContents()), //E1
							Integer.parseInt(nurseSheet.getCell(column + 1, row).getContents()), //E2
							Integer.parseInt(nurseSheet.getCell(column + 2, row).getContents()), //D1
							Integer.parseInt(nurseSheet.getCell(column + 3, row).getContents()), //D2
							Integer.parseInt(nurseSheet.getCell(column + 4, row).getContents()), //L1
							Integer.parseInt(nurseSheet.getCell(column + 5, row).getContents()), //L2
							Integer.parseInt(nurseSheet.getCell(column + 6, row).getContents()), //N1
							Integer.parseInt(nurseSheet.getCell(column + 7, row).getContents()), //N2
							Integer.parseInt(nurseSheet.getCell(column + 8, row).getContents()) //Free
						);
					prefereces.add(preference);
					column += 9;
				}
				
				nurse.setDayPrefences(prefereces);
				nurse.setType(Integer.parseInt(nurseSheet.getCell(column + 1, row).getContents()));
				nurse.setSwitchingPenalty(Integer.parseInt(nurseSheet.getCell(column + 2, row).getContents()));
				nurse.setDepartmentPenalty(Integer.parseInt(nurseSheet.getCell(column + 3, row).getContents()));
				nurses.add(nurse);
			}
			
		} catch(Exception ex) {
			ex.printStackTrace();
			throw new Exception("Bad structure or to large excel sheet :'" + nurseSheetName +"'");
		}	
		
		return nurses;
	}
	
	public List<Department> getDepartments() throws Exception {
		List<Department> departments = new ArrayList<Department>();
		
		try {
			Department department;
			for (int row = 2; row < departmentSheet.getRows(); row++) {
				department = new Department(departmentSheet.getCell(0, row).getContents());

				department.setAmountNurses
					(					
							Integer.parseInt(departmentSheet.getCell(1, row).getContents()), //E1
							Integer.parseInt(departmentSheet.getCell(2, row).getContents()), //E2
							Integer.parseInt(departmentSheet.getCell(3, row).getContents()), //D1
							Integer.parseInt(departmentSheet.getCell(4, row).getContents()), //D2
							Integer.parseInt(departmentSheet.getCell(5, row).getContents()), //L1
							Integer.parseInt(departmentSheet.getCell(6, row).getContents()), //L2
							Integer.parseInt(departmentSheet.getCell(7, row).getContents()), //N1
							Integer.parseInt(departmentSheet.getCell(8, row).getContents()) //N2
					);
				
				departments.add(department);
			}
			
		} catch(Exception ex) {
			ex.printStackTrace();
			throw new Exception("Bad structure or to large excel sheet :'" + departmentSheet +"'");
		}	
		
		return departments;
	}
}

